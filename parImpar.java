import java.util.Scanner;

public class parImpar {
    public static void main(String[] args) {
        int numero;
        Scanner entrada = new Scanner(System.in);
        System.out.print("Informe um número: ");
        numero = entrada.nextInt();
        if (numero > 0) {
            System.out.println("O numero " + numero + " é maior que 0.");
        } else if (numero == 0) {
            System.out.println("O numero é 0.");
        } else if (numero < 0) {
            System.out.println("O numero " + numero+ " é menor que 0.");
        }
    }
}

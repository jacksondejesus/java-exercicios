import java.util.Scanner;

public class matriz {
    public static void main(String[] args) {
        System.out.println("Maior valor da matriz.");

        Scanner entrada = new Scanner(System.in);
        
        System.out.print("Informe o número de linhas: ");
        int linhas = entrada.nextInt();

        System.out.print("Informe o número de colunas: ");
        int colunas = entrada.nextInt();

        int matriz[][] = new int[linhas][colunas];
        int maior = 0;

        System.out.println("Informe os valores da matriz:");
        System.out.println();
        for (int i = 0; i < linhas; i++) {
            for (int j = 0; j < colunas; j++) {
                System.out.print("Posição " + i + " " + j + ": ");
                matriz[i][j] = entrada.nextInt();

                if (matriz[i][j] > maior) {
                    maior = matriz[i][j];
                }
            }
        }
        System.out.println();
        System.out.println("Matriz gerada:");
        System.out.println();
        for (int i = 0; i < linhas; i++) {
            for (int j = 0; j < colunas; j++) {
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println("");
        }
        System.out.println();
        System.out.println("O maior valor da matriz é " + maior + "!");
        System.out.println();
    }
}
